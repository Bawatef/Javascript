




/*fs.appendFile('lorem.txt', 'Hello content!', function (err) {
  if (err) throw err;
  console.log('Saved!');
});*/

const hostname = '127.0.0.1';
const port = 3000;

let http = require('http')
let fs = require('fs')
let url = require ('url')

let server = http.createServer()

server.on('request', (request, response)=> {
  response.writeHead (200)
  let query = url.parse(request.url, true).query

/* Créer un document texte */
  fs.appendFile('lorem.txt', 'utf8', (err)=> {

      if (err) {

        response.writeHead(404)
        response.end("Ce fichier n'existe pas")

      } else {
        response.writeHead(200, {
          'content-type': 'text/html; charset=utf-8'
        })

      }
  })

if (fs.existsSync('example')){

}


/* Créer un fichier
fs.mkdir('example', (error)=> {
  console.log('Dir created');
}) */

/* copier un document

fs.copyFile('lorem.txt', './example/lorem.txt', (err) => {
  if (err) throw err;
  console.log('lorem.txt was copied to example');
}); */

/* Deleate file


fs.unlink('lorem.txt', function (err) {
  if (err) throw err;
  console.log('File deleted!');
}); */

/*Afficher un fichier*/
fs.readFile('/example/lorem.txt', (err, data) => {
  if (err) throw err;
  console.log(data);
});


})


server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
