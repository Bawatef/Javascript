ajaxGet("http://api.apixu.com/v1/current.json?key=2ca528b433f44a3f8cf130625180404&q=Paris", function (reponse) {

    var weather = JSON.parse(reponse);

    // Récupération de certains résultats

    var temperature = weather.current.temp_c;

    var humidite = weather.current.humidity;

    var imageUrl = weather.current.condition.icon;

    // Affichage des résultats

    var conditionsElt = document.createElement("div");

    conditionsElt.textContent = "Il fait actuellement " + temperature +

        "°C et l'humidité est de " + humidite;

    var imageElt = document.createElement("img");

    imageElt.src = imageUrl;

    var weatherElt = document.getElementById("weather");

    weatherElt.appendChild(conditionsElt);

    weatherElt.appendChild(imageElt);

});